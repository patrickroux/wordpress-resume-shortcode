#How to use it

##Just add the shortcode [itemcv] in your page content.

You can chose to share the amazing stuff you have done during this experience, by writing it between [itemcv] and [/itemcv].

##Some optional attributes are also available :

* start date : optional, start date of the experience (works together with enddate)
* enddate : optional, end date of the experience (works together with startdate)
* line1 : optional, for example the employer (works together with line2)
* line2 : optional, for example the position (works together with line1)

##For example

[itemcv startdate="January 2012" enddate="December 2013" line1="Apple Inc." line2="Chief Marketing Officer"] 
I have designed the new iPhone 7
[/itemcv]